import React, {Component} from "react";
import LinearProgress from "./LinearProgress";

export default class ImageQualityComp extends Component{
    render(){
        return(
            <div className="comp-wrap">
                {
                    this.props.data.dominantColors ?
                    <div className="res-item">
                        <div className="res-label">Dominant Colors</div>
                        <div className="res-val">
                            {
                                this.props.data.dominantColors.map((dc,index)=>{
                                    var colorToShow = "rgb( " + dc[0]+ "," + dc[1] + ", " + dc[2]+ ")";
                                    return(
                                        <div className="cc" key={index} style={{backgroundColor:colorToShow}}/>
                                    );
                                })
                            }
                        </div>
                    </div>
                    :
                    null
                }
                {
                    Object.keys(this.props.data).map((dc,index)=>{
                        let currentObj = this.props.data[dc];
                        if(dc !== "dominantColors" && dc != "resolution"){
                            return(
                                <div className="res-item half" key={index}>
                                    <div className="res-label">{dc}</div>
                                    <div className="res-text">
                                        {currentObj.text}
                                        <span>
                                            {
                                                " (" + (currentObj.score * 100).toFixed(1) + "%)"
                                            }
                                        </span>
                                    </div>
                                    <div className="res-val">
                                        <LinearProgress value={currentObj.score}/>
                                    </div>
                                </div>
                            )
                        } else if(dc == "resolution"){
                            var scoreToSend = 0;
                            if(currentObj.text == "Good resolution"){
                                scoreToSend=1;
                            } else if(currentObj.text == "Low resolution"){
                                scoreToSend=0.5;
                            } else {
                                scoreToSend=0.1;
                            }
                            return(
                                <div className="res-item half" key={index}>
                                    <div className="res-label">{dc}</div>
                                    <div className="res-text">
                                        {currentObj.text}
                                    </div>
                                    <div className="res-val">
                                        <LinearProgress value={scoreToSend}/>
                                    </div>
                                </div>
                            )
                        }
                    })
                }
            </div>
        )
    }
}