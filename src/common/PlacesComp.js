import React, {Component} from "react";
import LinearProgress from "./LinearProgress";

export default class PlacesComp extends Component{
    render(){
        return(
            <div className="comp-wrap">
                <div className="res-item">
                    <div className="res-label">Scene Attributes</div>
                    {
                        this.props.data && this.props.data.places && this.props.data.places.sceneAttributes ?
                            <div className="res-val">
                                {
                                    this.props.data.places.sceneAttributes.split(",").map((sa,idx)=>{
                                        return(
                                            <div className="rtag small" key={idx}>
                                                {sa}
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        :
                        null
                    }
                </div>
                {
                    this.props.data.places.sceneCategories.map((sc,idx)=>{
                        return(
                            <div className="res-item half" key={idx}>
                                <div className="res-label">{sc[0]}</div>
                                <div className="res-text">
                                    {
                                        " (" + (sc[1] * 100).toFixed(1) + "%)"
                                    }
                                </div>
                                <div className="res-val">
                                    <LinearProgress value={sc[1]}/>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        )
    }
}