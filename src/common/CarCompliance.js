import React, {Component} from "react";
import LinearProgress from "./LinearProgress";

export default class CarComplianceComp extends Component{
    render(){
        let imgToShow = "data:image/png;base64, " + this.props.data;
        return(
            <div className="comp-wrap">
                <div className="comp-title">Car Compliance</div>
                <div className="img-contee">
                    <img className="imgww" src={imgToShow}/>
                </div>
            </div>
        )
    }
}