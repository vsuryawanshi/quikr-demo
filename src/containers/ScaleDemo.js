import React, { Component } from "react";
import axios from "axios";
import Draggable from 'react-draggable';

import ImageQualityComp from "../common/ImageQualityComp";
import AestheticComp from "../common/AestheticComp";
import EnvironmentComp from "../common/EnvironmentComp";
import PlacesComp from "../common/PlacesComp";
import NSFWComp from "../common/NSFWComp";
import CarPoseComp from "../common/CarPose";
import CarComplianceComp from "../common/CarCompliance";
import CarModelComp from "../common/CarModel";

const MAIN_CATS = [
    {
        "name":"Car",
        "apistoshow":[
            {
                name:"Car Pose",
                color:"rgb(226, 62, 124)",
                url:"http://51.15.15.25:8010/api/v1/pose?url=",
                k:"carpose",
                bg:require("../images/scale/sedan.svg")
            },
            {
                name:"Car Compliance",
                color:"rgb(255, 255, 255)",
                url:"http://51.15.15.25:8012/api/v1/license?url=",
                k:"carcompliance",
                bg:require("../images/scale/compliance.png")
            },
            {
                name:"Car Model Recognition",
                color:"rgb(99, 179, 28)",
                url:"http://51.15.15.25:8012/api/v1/license?url=",
                k:"carmodel",
                bg:require("../images/scale/sedan.svg")
            }
        ]
    },
    {
        "name":"Content Moderation",
        "apistoshow":[
            {
                name:"NSFW Detection",
                color:"rgb(0,0,0)",
                url:"http://51.15.15.25:8008/api/v1/nsfw?url=",
                k:"nsfw",
                bg:require("../images/scale/nsfw.png")
            }
        ]
    },
    {
        "name":"Home",
        "apistoshow":[
            {
                name:"Image Quality API",
                color:"rgb(150, 49, 231)",
                url:"http://51.15.15.25:8002/api/v1/quality?url=",
                k:"iq",
                bg:require("../images/scale/quality.png")
            },
            {
                name:"Places API",
                color:"rgb(99, 179, 28)",
                url:"http://51.15.15.25:8002/api/v1/place?url=",
                k:"pla",
                bg:require("../images/scale/places.png")
            }
        ]
    }
];

const IMGS = [
    "http://62.210.93.54:9568/deeppulse/1.jpg",
    "http://62.210.93.54:9568/deeppulse/2.jpg",
    "http://62.210.93.54:9568/deeppulse/3.jpg",
    "http://62.210.93.54:9568/deeppulse/4.jpeg",
    "http://62.210.93.54:9568/deeppulse/5.jpg",
    "http://62.210.93.54:9568/deeppulse/6.jpg",
    "http://62.210.93.54:9568/deeppulse/7.jpg",
    "http://62.210.93.54:9568/deeppulse/8.jpg"
];

export default class ScaleDemo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            apiCallInProgress:false,
            selectedTypeIndex:0,
            selectedTabIndex:0,
            selectedImageIndex:-1,
            imgUrl:"",
            showJSONContent:false,
            currentImageData:null,
            showModal:false,
            currentBaseUrl:MAIN_CATS[0].apistoshow[0].url,
            showResults:false,
            selectedMainCategoryIndex:0,
            imgErr:""
        };
        this.imageUrlText = null;
    }

    makeApiCall(){
        this.setState({
            apiCallInProgress:true
        },()=>{
            axios({
                method:"GET",
                url:this.state.currentBaseUrl + "" +this.state.imgUrl
            }).then(response => {
                if(response.data.meta._code !== 200){
                    this.setState({
                        currentImageData:null,
                        apiCallInProgress:false,
                        showResults:false,
                        showJSONContent:false,
                        imgErr:response.data.meta.message
                    })
                } else {
                    this.setState({
                        currentImageData:response.data,
                        apiCallInProgress:false,
                        showResults:true,
                        imgErr:""
                    })
                }
            }).catch(err => {
                console.log(err);
                this.setState({
                    apiCallInProgress:false
                })
            })
        })
    }

    renderImageData(){
        let imageData = this.state.currentImageData.tags;
        let keyToUse = MAIN_CATS[this.state.selectedMainCategoryIndex].apistoshow[this.state.selectedTypeIndex].k;
        switch(keyToUse){
            case "iq":
                return <ImageQualityComp data={imageData} key={keyToUse}/>
            case "aes":
                return <AestheticComp data={imageData} key={keyToUse}/>
            case "env":
                return <EnvironmentComp data={imageData} key={keyToUse}/>
            case "pla":
                return <PlacesComp data={imageData} key={keyToUse}/>
            case "nsfw":
                return <NSFWComp data={imageData} key={keyToUse}/>
            case "carpose":
                return <CarPoseComp data={imageData} key={keyToUse}/>
            case "carcompliance":
                return <CarComplianceComp data={this.state.currentImageData.image} key={keyToUse}/>
            case "carmodel":
                return <CarModelComp data={this.state.currentImageData} key={keyToUse}/>
        }
    }

    validateUrlandMakeCall(){
        var pastedUrl = this.imageUrlText.value;
        this.binaryImageData = null;
        this.setState({
            imgUrl:pastedUrl,
            showModal:false,
        },()=>{
            this.makeApiCall();
        })
    }

    showUploadedItem (source) {
        this.setState({imgUrl:source});
    }

    makeUploadRequest(fdata){
        this.setState({
            apiCallInProgress:true
        },()=>{
            axios({
                method:"POST",
                url:this.state.currentBaseUrl.substring(0,this.state.currentBaseUrl.indexOf("?")),
                data:{"image":fdata}
            }).then(response =>{
                if(response.data.meta._code !== 200){
                    this.setState({
                        currentImageData:null,
                        apiCallInProgress:false,
                        showResults:false,
                        showJSONContent:false,
                        imgErr:response.data.meta._message
                    })
                } else {
                    this.setState({
                        currentImageData:response.data,
                        apiCallInProgress:false,
                        showResults:true,
                        imgErr:""
                    })
                }
            }).catch(err => {
                console.log(err);
                this.setState({
                    apiCallInProgress:false
                })
            })
        })
    }

    render() {
        return (
            <div className="home-wrapper">
                <div className="title">Our Products</div>
                <div className="subtitle">Using computer vision, our APIs extract the smallest details from an image. Providing you with valuable and useful insights.</div>

                <div className="main-cat-wrap">
                    {
                        MAIN_CATS.map((mc,index)=>{
                            return(
                                <div className={`main-cat`+(this.state.selectedMainCategoryIndex == index ? " selected" : "")} key={index} onClick={()=>{
                                    this.setState({
                                        selectedMainCategoryIndex:index,
                                        showResults:false,
                                        showJSONContent:false,
                                        currentImageData:null
                                    });
                                }} >
                                    {mc.name}
                                </div>
                            )
                        })
                    }
                </div>
                <ul className="type-wrap">
                    {
                        MAIN_CATS[this.state.selectedMainCategoryIndex].apistoshow.map((type,index)=>{
                            return(
                                <li 
                                    className={`type-item` + (this.state.selectedTypeIndex == index ? " active" : "")} 
                                    key={index}
                                    onClick={()=>{
                                        this.setState({
                                            selectedTypeIndex:index,
                                            currentBaseUrl:type.url,
                                            showResults:false,
                                            showJSONContent:false,
                                            currentImageData:null
                                        },()=>{
                                            if(this.binaryImageData !== null){
                                                this.makeUploadRequest(this.binaryImageData);
                                            } else {
                                                if(this.state.imgUrl !== ""){
                                                    this.makeApiCall();
                                                }
                                            }
                                        })
                                    }}>
                                    <div className="circle" style={{backgroundColor:type.color}}>
                                        <img src={type.bg} className="type-bg"/>
                                    </div>
                                    <div className="txt">{type.name}</div>
                                </li>
                            )
                        })
                    }
                </ul>

                <div className="main-section">
                    <div className="left-input">
                        <div className="image-input-container">
                            <div className="titlebar">
                                <div className="actions">
                                    <i className="b c"/>
                                    <i className="b min"/>
                                    <i className="b max"/>
                                </div>
                                <div className="ttext">Please choose an image</div>
                            </div>
                            <div className="default-images">
                                {
                                    IMGS.map((currentImg,index)=>{
                                        return(
                                            <div 
                                                className={`default-img` + (this.state.selectedImageIndex == index ? " active" : "")} 
                                                key={index} 
                                                style={{backgroundImage:"url(" + currentImg + ")"}}
                                                onClick={()=>{
                                                    this.binaryImageData = null;
                                                    this.setState({
                                                        selectedImageIndex:index,
                                                        imgUrl:IMGS[index],
                                                        showJSONContent:false,
                                                        showResults:false,
                                                        currentImageData:null
                                                    },()=>{
                                                        this.makeApiCall();
                                                    });
                                                }}/>
                                        )
                                    })
                                }
                            </div>
                        </div>
                        <div className="or">OR</div>
                        <button className="btn black big" style={{marginRight:20}} onClick={()=>{
                            this.setState({
                                showModal:true
                            });
                        }}>Enter Image URL</button>
                        <div className="upload">
                            <input type="file" name="file" id="file" className="inputfile" accept="image/*" onChange={(e)=>{
                                var _this = this;
                                if(e.target.files.length > 0){
                                    var file = e.target.files[0];
                                    var formdata;
                                    if (window.FormData) {
                                        formdata = new FormData();
                                    }
                                    if(window.FileReader){
                                        var reader = new FileReader();
                                        reader.onloadend = function (e) { 
                                            _this.showUploadedItem(e.target.result);
                                            if (formdata) {
                                                formdata.append("image", file);
                                            }
                                            var be = e.target.result.substring(e.target.result.indexOf(",")+1);
                                            _this.binaryImageData = be;
                                            _this.makeUploadRequest(_this.binaryImageData);
                                        };
                                        reader.readAsDataURL(file);
                                    }
                                }
                            }}/>
                            <label htmlFor="file">Upload an Image</label>
                        </div>
                    </div>
                    <div className="result-container">
                        {
                            this.state.imgUrl !== "" ?
                            <div className="current-selected-image">
                                <img src={this.state.imgUrl} className="mimg"/>
                                {
                                    this.state.apiCallInProgress ?
                                    <div className="loader">
                                        
                                    </div>
                                    :
                                    null
                                }
                                {
                                    this.state.imgErr !== "" ?
                                    <div className="ier">
                                        {this.state.imgErr}
                                    </div>
                                    :
                                    null
                                }
                                <button className="btn jbtn black" onClick={()=>{
                                    this.setState({
                                        showResults:true
                                    });
                                }}>Results</button>
                                <button className="btn jbtn" onClick={()=>{
                                    this.setState({
                                        showJSONContent:true
                                    });
                                }}>JSON</button>
                            </div>
                            :
                            null
                        }
                    </div>
                </div>
                <div className={`modal` + (this.state.showModal ? " show" : "")}>
                    <div className="mtitle">Enter Image URL</div>
                    <div className="mcontent">
                        <textarea className="imgurl" ref={node => this.imageUrlText = node} placeholder="Paste the image url here"/>
                    </div>
                    <div className="maction">
                        <button className="btn" onClick={()=>{
                            this.setState({
                                showModal:false
                            })
                        }} style={{marginRight:20}}>Cancel</button>
                        <button className="btn black" onClick={()=>{
                            this.validateUrlandMakeCall();
                        }}>Go</button>
                    </div>
                </div>
                <Draggable
                    handle=".handle"
                    defaultPosition={{x: 600, y: -100}}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                        <div className={`json-container` + (this.state.showJSONContent ? " show" : "")}>
                            <div className="titlebar">
                                <div className="actions">
                                    <i className="b c" onClick={()=>{
                                        this.setState({
                                            showJSONContent:false
                                        })
                                    }}/>
                                    <i className="b min"/>
                                    <i className="b max"/>
                                </div>
                                <div className="ttext handle">Response JSON</div>
                            </div>
                            <div className="json-content">
                                <pre className="javascript">
                                    {JSON.stringify(this.state.currentImageData, null, 4)}
                                </pre>
                            </div>
                        </div>
                </Draggable>
                <Draggable
                    handle=".ew"
                    defaultPosition={{x: 600, y: -100}}
                    onStart={this.handleStart}
                    onDrag={this.handleDrag}
                    onStop={this.handleStop}>
                        <div className={`results-container` + (this.state.showResults ? " show" : "")}>
                            <div className="titlebar">
                                <div className="actions">
                                    <i className="b c" onClick={()=>{
                                        this.setState({
                                            showResults:false
                                        })
                                    }}/>
                                    <i className="b min"/>
                                    <i className="b max"/>
                                </div>
                                <div className="ttext ew">Image Attributes</div>
                            </div>
                            {
                                this.state.currentImageData !== null?
                                <div className="result-content">
                                    {this.renderImageData()}   
                                </div>
                                :
                                null
                            }
                        </div>
                </Draggable>
            </div>
        );
    }
}